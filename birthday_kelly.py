import PanelController
import LEDAnimation

# LED Runner for door decorating contest

bday = LEDAnimation.LEDAnimation('gifs/bday_kelly.gif', 190, "horizontal")
shots = LEDAnimation.LEDAnimation('gifs/shots.gif', 174, "horizontal")
anchor = LEDAnimation.LEDAnimation('gifs/anchor_dg.gif', 139, "horizontal")
animations =  [(bday, 0.05), (shots, 0.05), (anchor, 0.05)]
p = PanelController.PanelController(animations, 20.0, True)
p.start()
