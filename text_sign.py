import PanelController
import LEDAnimation

# LED Runner for door decorating contest

words = LEDAnimation.LEDAnimation('moving_text.gif', 154, "horizontal")
mikes = LEDAnimation.LEDAnimation('mikes.gif', 154, "horizontal")
animations =  [(words, 0.1), (mikes, 0.1)]
p = PanelController.PanelController(animations, 30.0, True)
p.start()