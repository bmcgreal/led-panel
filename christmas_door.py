import PanelController
import LEDAnimation

# LED Runner for door decorating contest

snowflakes = LEDAnimation.LEDAnimation('gifs/snowflakes_animation.gif', 31, "vertical")
tree = LEDAnimation.LEDAnimation('gifs/christmas_tree.gif', 14, "vertical")
snowman = LEDAnimation.LEDAnimation('gifs/snowman.gif', 4, "vertical")
animations =  [(snowflakes, 0.3), (tree, 0.3), (snowman, 0.2)]
p = PanelController.PanelController(animations, 30.0)
p.start()
