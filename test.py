from PIL import Image, ImageDraw, ImageFont

font = ImageFont.truetype('Shaston320.ttf', size=8)

sentence = "bruh sentence"
height = 12
width = len(sentence)*8 - sentence.count(' ')*3
img = Image.new('RGB', (width, height), 'black')

draw = ImageDraw.Draw(img, 'RGB')
draw.text((-10,2), sentence, font=font, fill='red')
img.save('text.png')