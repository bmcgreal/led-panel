import PanelController
import LEDAnimation

# LED Runner for door decorating contest

bar = LEDAnimation.LEDAnimation('bar.gif', 4, "horizontal")
animations =  [(bar, 0.2)]
p = PanelController.PanelController(animations, 30.0, True)
p.start()