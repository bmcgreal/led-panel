import LEDAnimation
import LEDPanel
import sys
import signal
import time
from gpiozero import Button

class PanelController:

    def __init__(self, animations_times, max_time=30, continuous=False):
        self.animations = [(None, 0)]
        for at in animations_times:
            self.animations.append(at)
        self.panel = LEDPanel.LEDPanel()
        self.num_animations = len(animations_times)
        self.max_time = max_time # want this to possibly be infinite
        self.button = Button(7, bounce_time=0.2)
        self._setup_button()
        self.starttime = 0.0
        self.continuous = continuous
    
    def start(self):
        self.anim_select = 0
        self.run()

    def run(self):
        while True:
            if self.max_time != -1:
                if (time.time() - self.starttime > self.max_time) and self.anim_select != 0:
                    if self.anim_select == self.num_animations:
                        if self.continuous:
                            self.anim_select = 1
                        else:
                            self.anim_select = 0
                    else:
                        self.anim_select += 1
                    self.starttime = time.time()
                    self._set_animation()
            self.play()

    def play(self):
        if self.anim_select == 0:
            self.panel.stop_board()
        else:
            self.panel.play_board()
            time.sleep(self.animations[self.anim_select][1])

    def set_next_animation(self):
        self.anim_select = self.anim_select + 1
        self.starttime = time.time()
        if self.anim_select > self.num_animations:
            self.anim_select = 0
        self._set_animation()

    def _set_animation(self):
        self.panel.set_animation(self.animations[self.anim_select][0])

    def _setup_button(self):
        self.button.when_pressed = self._button_pressed

    def _button_pressed(self, channel):
        self.panel.stop_board()
        self.anim_select = self.anim_select + 1
        self.starttime = time.time()
        if self.anim_select > self.num_animations:
            self.anim_select = 0
        self._set_animation()
