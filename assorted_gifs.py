import PanelController
import LEDAnimation

# LED Runner for door decorating contest

car = LEDAnimation.LEDAnimation('gifs/car.gif', 52, "horizontal")
pokemon = LEDAnimation.LEDAnimation('gifs/pokemon.gif', 32, "horizontal")
shark = LEDAnimation.LEDAnimation('gifs/shark.gif', 15, "horizontal")
meteor = LEDAnimation.LEDAnimation('gifs/meteor.gif', 9, "horizontal")
nyancat = LEDAnimation.LEDAnimation('gifs/nyancat.gif', 2, "horizontal")
animations =  [(car, 0.05), (pokemon, 0.05), (shark, 0.05), (meteor, 0.05), (nyancat, 0.05)]
p = PanelController.PanelController(animations, 15.0, True)
p.start()
