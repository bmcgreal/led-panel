from PIL import Image, ImageDraw, ImageFont, ImageSequence

font = ImageFont.truetype('Shaston320.ttf', size=8)

def create_image_with_text(wh, text):
    width, height = wh
    img = Image.new('RGB', (25, 12), "black")
    draw = ImageDraw.Draw(img)
    # draw.ellipse takes a 4-tuple (x0, y0, x1, y1) where (x0, y0) is the top-left bound of the box
    # and (x1, y1) is the lower-right bound of the box.
    draw.text((width, height), text, font = font, fill=color)
    return img

sentence = input("input a sentence to gif-ize: ")
name = input("input filename: ")
color = input("input color: ")
filename = 'gifs/' + name + '.gif'

# Create the frames
frames = []
x, y = 30, 0
max_len = len(sentence)*8
for i in range(max_len+25):
    new_frame = create_image_with_text((x-i,2), sentence)
    frames.append(new_frame)

tm = len(sentence)*2
frames[0].save(filename, format='GIF', append_images=frames[1:], save_all=True, loop=0)

text_gif = Image.open(filename)
im = ImageSequence.Iterator(text_gif)
length = 0
for ims in im:
    length += 1
print(length)
