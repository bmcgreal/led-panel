from PIL import Image, ImageDraw, ImageFont, ImageSequence

class PNGText:

    def _init__(self):
        pass

    def create_png(self, text, filename, color):
        font = ImageFont.truetype('Shaston320.ttf', size=6)
        self.img = Image.new('RGB', (30, 12), "black")
        draw = ImageDraw.Draw(self.img)
        draw.text((0,2), text, font = font, fill=color)
        filename = 'pngs/' + filename + '.png'
        self.img.save(filename, "PNG")

    def get_img(self):
        return self.img
