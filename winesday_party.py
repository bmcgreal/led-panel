import PanelController
import LEDAnimation

# LED Runner for door decorating contest

bar = LEDAnimation.LEDAnimation('gifs/bar.gif', 4, "horizontal")
winesday = LEDAnimation.LEDAnimation('gifs/winesday.gif', 84, "horizontal")
coldbeers = LEDAnimation.LEDAnimation('gifs/beer.gif', 124, "horizontal")
shots = LEDAnimation.LEDAnimation('gifs/shots.gif', 169, "horizontal")
shots = LEDAnimation.LEDAnimation('gifs/Thunderdrunkgif', 169, "horizontal")
animations =  [(winesday, 0.1), (bar, 0.2), (coldbeers, 0.1), (shots, 0.1), (Thunderdrunk, 0.1)]
p = PanelController.PanelController(animations, -1, True)
p.start()
