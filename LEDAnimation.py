import neopixel
import board
from PIL import Image, ImageSequence

class LEDAnimation:

    def __init__(self, filepath, max_frame, orientation):
        self.image = Image.open(filepath)
        self.ranges = [(297,272), (273,248), (247,223), (224,200), (199,174), (175,150), (149,124), (125,100), (99,74), (74,50), (49,24), (25,0)]
        self.prepixels = []
        for i in range(298):
            self.prepixels.append((0,0,0))
        self.frame = neopixel.NeoPixel(board.D18, 298, auto_write=False)
        self.im_iterator = ImageSequence.Iterator(self.image)
        self.frame_number = 0
        self.max_frame = max_frame
        self.orientation = orientation
    
    def get_next_frame(self):
        if self.orientation == "vertical":
            self._generate_frame_vertical()
        else:
            self._generate_frame_horizontal()
        self._power_correct()
        self._fill_frame()
        return self.frame
    
    def get_frame(self):
        return self.frame

    # Fills self.prepixels with new frame
    def _generate_frame_vertical(self):
        rgb_image = self._next_image()
        direction = True
        x = 0
        for r in self.ranges:   
            self._fill_column(rgb_image, r, x, direction)
            x += 1
            direction = not direction

    # Fills a single column, correcting for the weird directioning of the board
    def _fill_column(self, rgb_image, array_range, x, direction):
        y = 24
        if array_range == (224,200):
            y = 23
        if direction:
            for i in range(array_range[0], array_range[1], -1):
                rgb = rgb_image.getpixel((x,y))
                self.prepixels[i] = rgb
                y -= 1
        else:
            for i in range(array_range[1], array_range[0], 1):
                rgb = rgb_image.getpixel((x,y))
                self.prepixels[i] = rgb
                y -= 1

    # Fills self.prepixels with new frame
    def _generate_frame_horizontal(self):
        rgb_image = self._next_image()
        direction = True
        y = 0
        for r in self.ranges:   
            self._fill_row(rgb_image, r, y, direction)
            y += 1
            direction = not direction

    # Fills a single row, correcting for the weird directioning of the board
    def _fill_row(self, rgb_image, array_range, y, direction):
        x = 0
        if array_range == (224,200):
            x = 1
        if direction:
            for i in range(array_range[0], array_range[1], -1):
                rgb = rgb_image.getpixel((x,y))
                self.prepixels[i] = rgb
                x += 1
        else:
            for i in range(array_range[1], array_range[0], 1):
                rgb = rgb_image.getpixel((x,y))
                self.prepixels[i] = rgb
                x += 1

    # Corrects power to ensure fewer than 1A are drawn
    # Dims the LEDs evenly
    def _power_correct(self):
        current = 0
        for r,g,b in self.prepixels:
            current += (r/255)*0.02 + (g/255)*0.02 + (b/255)*0.02

        scaler = 0.5
        if current >= scaler:
            scaler = scaler/current

        for i in range(298):
            r,g,b = self.prepixels[i]
            self.prepixels[i] = (int(r*scaler), int(g*scaler), int(b*scaler))

    # Fills the actual frame that will be returned
    def _fill_frame(self):
        for i in range(298):
            self.frame[i] = self.prepixels[i]

    # Gets the next image that will be converted to the panel as an RGB type
    def _next_image(self):
        frame = self.im_iterator[self.frame_number]
        rgb = frame.convert('RGB')
        self.frame_number = self.frame_number + 1
        if self.frame_number == self.max_frame:
            self.frame_number = 0
        return rgb

