import PanelController
import LEDAnimation

# LED Runner for door decorating contest

pub = LEDAnimation.LEDAnimation('gifs/pub_405.gif', 8, "horizontal")
keg = LEDAnimation.LEDAnimation('gifs/keg.gif', 4, "horizontal")
bar = LEDAnimation.LEDAnimation('gifs/bar.gif', 4, "horizontal")
animations =  [(pub, 0.2), (keg, 0.2), (bar, 0.2)]
p = PanelController.PanelController(animations, -1, True)
p.start()
