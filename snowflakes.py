import neopixel
import board
import PIL.Image


snowflake_png = PIL.Image.open('snowflakes_static.png')
snowflake_rgb = snowflake_png.convert('RGB')

prepixels = []
for i in range(298):
    prepixels.append((0,0,0))

pixels = neopixel.NeoPixel(board.D18, 298, auto_write=False)

pixels.fill((0,0,0))
pixels.show()

ranges = [(297,272), (273,248), (247,223), (224,200), (199,174), (175,150), (149,124), (125,100), (99,74), (74,50), (49,24), (25,0)]


def fill_column(array_range, x, direction):
    y = 24
    if array_range == (224,200):
        y = 23
    if direction:
        for i in range(array_range[0], array_range[1], -1):
            rgb = snowflake_rgb.getpixel((x,y))
            prepixels[i] = rgb
            y -= 1
    else:
        for i in range(array_range[1], array_range[0], 1):
            rgb = snowflake_rgb.getpixel((x,y))
            prepixels[i] = rgb
            y -= 1

x = 0
direction = True
for r in ranges:
    fill_column(r, x, direction)
    x += 1
    direction = not direction

current = 0
for r,g,b in prepixels:
    current += 0.02*r + 0.02*g +  0.02*b 

scaler = 1
if current >= 3:
    scaler = 3/current

for i in range(298):
    r,g,b = prepixels[i]
    prepixels[i] = (int(r*scaler), int(g*scaler), int(b*scaler))
    pixels[i] = prepixels[i]

pixels.show()
