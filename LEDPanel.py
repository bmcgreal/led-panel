import neopixel
import board
import LEDAnimation
import time

class LEDPanel:

    def __init__(self):
        self.pixels = neopixel.NeoPixel(board.D18, 298, auto_write=False)

    def set_animation(self, animation):
        self.animation = animation

    def play_board(self):
        self.pixels = self.animation.get_next_frame()
        self.pixels.show()
    
    def stop_board(self):
        self.pixels.fill((0,0,0))
        self.pixels.show()
