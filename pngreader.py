from PIL import Image

snowflake_png = Image.open('snowflakes_static.png')
snowflake_rgb = snowflake_png.convert('RGB')

for x in range(12):
    for y in range(25):
        print(snowflake_rgb.getpixel((x,y)))