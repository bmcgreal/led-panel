from text_to_png import PNGText
import PanelController
import LEDAnimation

# LED Runner for door decorating contest

score = input("input score: ")

png_creator = PNGText()
png_creator.create_png(score, "score", "blue")

scoreboard = LEDAnimation.LEDAnimation('pngs/score.png', 1, "horizontal")
animations =  [(scoreboard, 0.1)]
p = PanelController.PanelController(animations, -1, True)
p.start()