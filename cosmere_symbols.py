import PanelController
import LEDAnimation

# LED Runner for door decorating contest

lerasium = LEDAnimation.LEDAnimation('lerasium.gif', 31)
animations =  [(lerasium, 0.05)]
p = PanelController.PanelController(animations, 30)
p.start()